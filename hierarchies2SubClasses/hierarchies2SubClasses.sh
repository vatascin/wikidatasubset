# Get wikidata concepts including their hierarchies up to second level subclasses
sparql_loop \
	https://query.wikidata.org/sparql \
	hierarchies2SubClasses.mustache \
	< hierarchies2SubClasses.csv \
	> hierarchies2SubClasses.n3