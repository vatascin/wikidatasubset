# Get wikidata concepts including their hierarchies (subclasses)
sparql_loop \
	https://query.wikidata.org/sparql \
	hierarchies.mustache \
	< hierarchies.csv \
	> hierarchies.n3