# Get wikidata concepts including their direct subclasses
sparql_loop \
	https://query.wikidata.org/sparql \
	hierarchies1SubClass.mustache \
	< hierarchies1SubClass.csv \
	> hierarchies1SubClass.n3