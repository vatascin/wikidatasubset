# Get wikidata concepts
sparql_loop \
	https://query.wikidata.org/sparql \
	concepts.mustache \
	< concepts.csv \
	> concepts.n3